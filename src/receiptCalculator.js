class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (productsList) {
    let sum = 0;
    const productsPriceList = [];
    if (productsList.length === 0) {
      return 0;
    }
    for (let i = 0; i < productsList.length; i++) {
      for (let j = 0; j < this.products.length; j++) {
        if (this.products[j].id === productsList[i]) {
          productsPriceList.push(this.products[j].price);
        }
      }
    }
    if (productsPriceList.length === 0) {
      throw new Error('Product not found.');
    }
    for (let i = 0; i < productsPriceList.length; i++) {
      sum += productsPriceList[i];
    }
    return sum;
  }
  // --end->
}

export default RecepitCalculator;
